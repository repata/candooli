
const mongoose = require('mongoose');

var settings = require('./settings')
	, shortid = require('shortid');


require('./models/config')();
const db = mongoose.connect(settings.db);

mongoose.connection.on('connected', function(er){
	console.log('Database connected.');
});
var data = {
	challenge: db.model('Challenge')
}



function rint(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}


var c = new data.challenge();
c.id = shortid.generate();
c.name = 'Lets Get Back Into Shape';
c.type = 'Group';
c.status = 'In Progress';
c.ownerUserId = '101';
c.players = [];
c.startDate = new Date(2014,7,10);
c.endDate = new Date(2014,8,30);
c.players.push({id:shortid.generate(), userId:'111', name:'Chris Trailer', canDos:[]});
c.players[0].canDos.push({
	id:shortid.generate()
	, description:'Workout'
	, frequency:3
	, frequencyType:'week'
	, minutes:45
	, doneIts:[
		{id:shortid.generate(), date: new Date(2014, 7, 20), minutes:60, note:''}
		, {id:shortid.generate(), date: new Date(2014, 7, 21), minutes:45, note:''}
		, {id:shortid.generate(), date: new Date(2014, 7, 22), minutes:45, note:''}
		, {id:shortid.generate(), date: new Date(2014, 7, 23), minutes:45, note:''}
		, {id:shortid.generate(), date: new Date(2014, 7, 19), minutes:45, note:''}
	]
});

c.players[0].canDos.push({
	id:shortid.generate()
	, description:'Read'
	, frequency:5
	, frequencyType:'week'
	, minutes:10
	, doneIts:[
		{id:shortid.generate(), date: new Date(2014, 7, 18), minutes:10, note:''}
		, {id:shortid.generate(), date: new Date(2014, 7, 19), minutes:10, note:''}
	]
});

var names = ['Zach Ott','Jake Kaffunizec','Julie Swenson', 'Brian Swenson', 'Kaite Warlock']
for (var i = 0; i < names.length; i++) {
	c.players.push({id:shortid.generate(), userId:'10'+i, name:names[i], canDos:[]});

	var pix = c.players.length -1;
	c.players[pix].canDos.push({
		id:shortid.generate()
		, description: i % 2 == 0? 'Read' : 'Read Magazine'
		, frequency:1
		, frequencyType:'day'
		, minutes:rint(10,45)
		, doneIts:[]
	});
	var daystart = rint(10,20)
	for (var a = 0; a < rint(3,8); a++) {
		c.players[pix].canDos[0].doneIts.push({id:shortid.generate(), date: new Date(2014, 7, a+daystart), minutes:10, note:a.toString()})
	};

	c.players[pix].canDos.push({
		id:shortid.generate()
		, description:i % 2 != 0 ? 'Workout' : 'Go to the gym'
		, frequency:rint(2,4)
		, frequencyType:'week'
		, minutes:rint(30,60)
		, doneIts:[
			
		]
	});

	daystart = rint(10,20);
	for (var a = 0; a < rint(3,8); a++) {
		c.players[pix].canDos[1].doneIts.push({id:shortid.generate(), date: new Date(2014, 7, a+daystart), minutes:rint(30,60), note:a.toString()})
	};

	if (i % 2 === 0){
		c.players[pix].canDos.push({
			id:shortid.generate()
			, description:'Write in journal'
			, frequency:rint(2,5)
			, frequencyType:'week'
			, minutes:rint(10,30)
			, doneIts:[
				
			]
		});

		daystart = rint(10,20);
		for (var a = 0; a < rint(3,8); a++) {
			c.players[pix].canDos[2].doneIts.push({id:shortid.generate(), date: new Date(2014, 7, a+daystart), minutes:rint(30,60), note:a.toString()})
		};
	}

};







c.save(function(er){

	console.log('done: ' + er);
});


// data.challenge.find({'players.userId':'111'}).exec(function(er, dat){
// 	console.log(dat.length + ' challenges')
// 	for (var i = 0; i < dat.length; i++) {
// 		console.log(dat[i].name);
// 		var user = dat[i].players.filter(function (player) {
// 		    return player.userId === '111';
// 		  }).pop();

// 		var doneIts = []; // 
// 		for (var b = 0; b < user.canDos.length; b++) {
// 			var cando = user.canDos[b];
// 			for (var a = 0; a < cando.doneIts.length; a++) {
// 				var di = cando.doneIts[a];
// 				di.canDo = cando.description;
// 				di.canDoId = cando.id;
// 				doneIts.push(di);
// 			};
// 		}; 

// 		console.log('\t' + user.canDos.length + ' CanDos');
// 		console.log('\t' + doneIts.length + ' Done Its');
// 	};

// 	console.log('done');
// })




