
var challenge = {
	canDos:[]
	, name:null
	, invites:[]
	, start:null
	, duration:0
};

var elms = {
	cando:{
		description:null
		, frequency:null
		, frequencyType:null
		, minutes:null
	}
}


elms.cando.description = $('#cando-description');
elms.cando.frequency = $('#cando-frequency');
elms.cando.frequencyType = $('#cando-frequencyType');
elms.cando.minutes = $('#cando-minutes');

$('#start').datepicker();

function genCanDo(cando){
	var s = '<li class="list-group-item"><h4 class="list-group-item-heading">'
		+ cando.description + '</h4>'
		+ '<p class="list-group-item-text">'
		+ 'for <b>' + cando.minutes + '</b> minutes <b>'
		+ cando.frequency + '</b> ' + (cando.frequency == 1 ? ' time ' : ' times ')
		+ 'per <b>' + cando.frequencyType + '</b></p></li>';

	return s;
}

$('#btn-step1').on('click', function(){

	//validate candos

	if (challenge.canDos.length > 0)
		$('#tab-steps a[href="#tab-challenge"]').tab('show');
	else
		alert('You need to enter at least one Can Do (Goal)');
})
$('#btn-step2').on('click', function(){
	var name = $('#name').val();
	var start = $('#start').val();
	var duration = $('#duration').val();
	var errors = [];
	//TODO validation
	if (name.length < 5)
		errors.push('The challenge name is too short.');
	try {
		var d = new Date(start);
	}
	catch (ex){
		errors.push('Invalid start date');
	}
	if (errors.length == 0){
		challenge.name = name;
		challenge.start = start;
		challenge.duration = duration;
		$('#tab-steps a[href="#tab-invites"]').tab('show');
	}
	else {
		alert(errors.join('\n'));
	}
		

	
});

$('#btn-finish').on('click', function(){
	//TODO validate challenge setup

	saveChallenge();
});

$('#btn-invite').on('click', function(){
	var s = $('#invite').val();
	function finish(s){
		$('#invitelist').append('<li class="list-group-item">' + s + '</li>');
		$('#invite').focus().val('');
	}
	if (validateEmail(s)){
		challenge.invites.push({email:s});
		finish(s);
	}
	else {
		s = s.replace(/[^0-9]+/g,'');
		if (s.length == 10 || (s.length == 11 && s.substring(0,1) == '1')){
			challenge.invites.push({mobile:s});
			var sd = s.substring(0,3)+'-'+s.substring(3,6) + '-' + s.substring(6);
			if (s.length == 11){
				sd = s.substring(1,3)+'-'+s.substring(4,7) + '-' + s.substring(7);
			}
			finish(sd);
		}
		else {
			alert('Not a valid email or mobile phone number');
		}
	}
	//TODO validate email or mobile
});
function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 
function saveChallenge(){
	$.ajax({
		url:'/api/challenge'
		, type:'POST'
		, dataType:'json'
		, data:challenge
		, success:function(d){
			alert(JSON.stringify(d));
		}
		, error: function(er){
			alert('There was a problem talking to the server. Try again.')
		}
	})
}

$('#btn-addcando').on('click', function(){
	//TODO store the input in objects
	var d = elms.cando.description.val()
		, f = elms.cando.frequency.val()
		, ft = elms.cando.frequencyType.val()
		, m = elms.cando.minutes.val();

	//TODO validate

	var cando = {
		description:d 
		, frequency:f 
		, frequencyType:ft 
		, minutes:m
	}

	challenge.canDos.push(cando);
	$('#candos li.none').remove();
	$('#candos').append(genCanDo(cando));

	elms.cando.description.val('');
	elms.cando.frequency.val('');
	elms.cando.minutes.val('');
	//$('#cando-description').val('');

	elms.cando.description.focus();
})


var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        // the typeahead jQuery plugin expects suggestions to a
        // JavaScript object, refer to typeahead docs for more info
        matches.push({ value: str });
      }
    });

    cb(matches);
  };
};

var states = [
	'Workout'
	, 'Read'
	, 'Yoga'
	, 'Pilates'
	, 'Go for a ride'
	, 'Play basketball'
	, 'Training'
	, 'Swim'
	, 'Excersize'
	, 'Run'
	, 'Jog'
];

$('.typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'states',
  displayKey: 'value',
  source: substringMatcher(states)
});