
//$('#di-date').datepicker({endDate:new Date()});
$('#challenge-start').datepicker({});
function addDoneIt(canDoId, date, minutes, note){
	$.ajax({
		url:'/api/challenge/' + challengeId + '/cando/' + canDoId + '/doneit'
		, type:'POST'
		, data:{
			date:date
			, minutes:minutes 
			, note:note
		}
		, dataType:'json'
		, success:function(d){
			if (!d.success) {
				alert(d.message || 'There was an odd problem saving your Done It.  Try again');
			}
			else {
				$('#modal-doneit').modal('hide');

				updatePlayers(d.players);

			}
		}
		, error:function(s, x, r){
			alert('Error: ' + s);
		}
	});
}


function updatePlayers(players){
	for (var i = 0; i < players.length; i++) {
		var p = players[i];
		$('#pl-' + p.userId).data('rank') = p.rank;
		$('#rank-' + p.userId).text(p.rank);
		$('#tp-' + p.userId).text(Math.floor(p.totalPercent*100));
	};
	$('#players li').sort(function(a,b){
		return parseInt($(a).data('rank')) < parseInt($(a).data('rank')) ? -1 : 1;
	})
	//TODO sort by rank
}

$('#btn-di-add').on('click', function(){
	var canDoId = $('#di-cando').val()
		, date = $('#di-date').val()
		, minutes = $('#di-minutes').val()
		, note = $('#di-note').val();

	addDoneIt(canDoId, date, minutes, note); 
});

var selectedId = null;
$('.edit-cando').on('click', editCandoClick);

function editCandoClick(){
	
	var id = $(this).data('candoid')
		, d = $(this).data('description')
		, f = $(this).data('frequency')
		, ft = $(this).data('frequencytype')
		, m = $(this).data('minutes');
	selectedId = id;
	$('#cando-description').val(d);
	$('#cando-frequency').val(f);
	$('#cando-frequencyType').val(ft);
	$('#cando-minutes').val(m);

	$('#modal-cando').modal('show');
}

$('.delete-cando').on('click', deleteCandoHandler);

function deleteCandoHandler(){
	var id = $(this).data('candoid');
	//TODO confirm modal
	$.ajax({
		url:'/api/challenge/' + challengeId + '/cando/' + id
		, dataType:'json'
		, type:'DELETE'
		, success:function(d){
			//TODO remove from UI
			if (d.success){
				$('#cando-' + id).remove();
			} else {
				alert(d.message || 'Unable to delete cando.')
			}
		}
		, error:function(s,x,r){
			alert('Error talking to server.' + s);
		}
	});
}

$('#btn-cando-cancel').on('click',function(){

		$('#cando-description').val('');
		$('#cando-frequency').val('');
		//$('#cando-frequencyType').val(ft);
		$('#cando-minutes').val('');

		selectedId = null;
})
$('#btn-cando-save').on('click',function(){
	var d = $('#cando-description').val()
		, f = $('#cando-frequency').val()
		, ft = $('#cando-frequencytype').val()
		, m = $('#cando-minutes').val();

	//TODO validate

	$.ajax({
		url:'/api/challenge/' + challengeId + '/cando' + (selectedId == null ? '' : ('/'+selectedId))
		, type:selectedId == null ? 'POST' : 'PUT'
		, dataType:'json'
		, data:{
			description:d 
			, frequency: f 
			, frequencyType: ft 
			, minutes:m
		}
		, success:function(dat){
			if (dat.success){

				var html = '<li class="list-group-item" id="cando-' + dat.newId + '">'
					+ '<div class="cando-num">0</div>'
					+ '<div class="pull-right" style="width:40px;">'
						+ '<div><a class="edit-cando" href="javascript:void(0);" onclick="editCandoClick()" data-candoid="' + dat.newId + '" data-description="' + d + '" data-frequency="' 
						+ f + '" data-frequencytype="' + ft + '" data-minutes="' + m + '">Edit</a></div>'
						+ '<div><a class="delete-cando" href="javascript:void(0);" data-candoid="' + dat.newId + '">Delete</a></div></div>'
						+ '<h4 class="list-group-item-heading">' + d + '</h4>'
						+ '<p class="list-group-item-text">for <b>' + m + '</b> minutes <b>' + f + '</b> times per <b>' + ft + '</b></p>'
					+ '</li>';
				var b = $(html);
				b.find('.edit-cando').on('click',editCandoClick);
				b.find('.delete-cando').on('click', deleteCandoHandler);
				$('#canlist').append(b);
				$('#canlist .none').remove();
				$('#cando-description').focus().val('');
				$('#cando-frequency').val('');
				
				$('#cando-minutes').val('');
			}
			else {
				alert(dat.message || 'Error saving Can Do.');
			}
		}
	})
});

$('#btn-invite').on('click', function(){
	var s = $('#invite').val();
	var invite = null;
	if (validateEmail(s)){
		invite = {email:s};
		//$('#invitelist').append('<li class="list-group-item">' + s + '</li>');
	}
	else {
		s = s.replace(/[^0-9]+/g,'');
		if (s.length == 10 || (s.length == 11 && s.substring(0,1) == '1')){
			invite = {mobile:s};
		}
	}
	//console.log(invite);
	if (invite != null){
		$.ajax({
			url:'/api/challenge/' + challengeId + '/invite'
			, type:'POST'
			, dataType:'json'
			, data:invite
			, success:function(d){
				if (!d.success)
					alert(d.message || 'There was a problem with the invite.');
				//TODO display msg on error
			}
			, error:function(s,x,r){
				alert('There was an error talking to the server.');
			}
		})
	}
	else {
		alert('Not a valid email or mobile phone number');
	}
});

$('#btn-challenge-save').on('click', function(){
	var name = $('#challenge-name').val()
		, start = $('#challenge-start').val()
		, duration = parseInt($('#challenge-duration').val());

	$.ajax({
		url:'/api/challenge/' + challengeId 
		, type:'PUT'
		, dataType:'json'
		, data: {name:name, start:start, duration:duration}
		, success:function(d){
			document.location.reload();
		}
		, error: function(s,x,r){
			alert('Error connecting to server');
		}
	})

});

$(document).ready(function(){
	var mine = $('#canlist').find('.none');
	if (mine.length == 1){
		$('#modal-nocandos').modal('show');
	}

	$('.hastooltip').tooltip();
})

function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 



function drawCal(doneIts){
	var html = ['<table class="cal-sm" cellpadding="2"><thead><tr><td>S</td><td>M</td><td>T</td><td>W</td><td>T</td><F</td><td>S</td></tr></thead><tbody>'];
	for (var i = 0; i < 7; i++) {
		
	};
	html.push('</tbody></table>');
}