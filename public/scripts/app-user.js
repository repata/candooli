

$(document).ready(function() {
    $('.formval').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The name is required and cannot be empty'
                    },
                    stringLength: {
                        min: 4,
                        message: 'The name must be more than 4 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_ ]+$/,
                        message: 'The name can only consist of alphabetical, number and underscore'
                    }
                }
            },
            username: {
                validators: {
                    notEmpty: {
                        message: 'The email is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
            , mobile:{
            	validators:{
            		phone:{message:'Invalid phone number.'}
            	}
            }
            , password:{
            	validators:{
            		stringLength:{
            			min:6
            			, message:'Your password must be at least 6 characters long'
            		}
            	}
            }
        }
    });
});

