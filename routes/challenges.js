var express = require('express');
var router = express.Router();
var utils = require('../lib/utils');
var db = require('../models/data');
var processor = require('../lib/processor');

require('date-utils');

/* GET chanllenges listing. */
router.get('/',utils.ensureAuthenticated, function(req, res) {
	var user = req.user;

	db.challenge.find({'players.userId':user.id}, function(er, data){
		//TODO handle err
		for (var i = 0; i < data.length; i++) {
			for (var a = 0; a < data[i].players.length; a++) {
				if (data[i].players[a].userId == user.id){
					data[i].userIx = a;
					break;
				}
			};
			
		};
		res.render('challenge-list',{title:'Challenges', user:req.user, challenges:data, userChallenges:data});

	});

  	
});


/* GET chanllenges listing. */
router.get('/new',utils.ensureAuthenticated, function(req, res) {
	var user = req.user;

	db.userChallenges(user.id, function(er, data){
		//TODO handle err
		var candos = [];
		for (var i = 0; i < data.length; i++) {
			var cl = data[i];
			var player = cl.players.filter(function(p){
				return p.userId == user.id;
			}).pop();

			for (var a = 0; a < player.canDos.length; a++) {
				candos.push(player.canDos[a]);
			};
		};
		res.render('challenge-new',{title:'New Challenge', user:req.user, canDos:candos, userChallenges:data});

	});

  	
});


function currentDoneIts(players){
	var today = Date.today()
		, thisWeekStart = Date.today().addDays(today.getDay()*-1);
		console.log('thisweekestart');
		console.log(thisWeekStart)
	for (var i = 0; i < players.length; i++) {
		var p = players[i];
		

		for (var c = 0; c < p.canDos.length; c++) {
			var cdic = 0;
			var can = p.canDos[c];
			var res = can.doneIts.filter(function(di){
				if (can.frequencyType == 'day')
					return today.equals(di.date);
				else if (can.frequencyType == 'week')
					return di.date.between(thisWeekStart, today);
			});
			
			if (res.length > 0) {
				cdic = res.length > can.frequency ? can.frequency : res.length;
			}
			players[i].canDos[c].currentDoneItCount = cdic;
		};

		
	};

	return players;
}

function recentActivity(players){
	var result = [];
	var today = Date.today()
		, start = Date.today().addDays(-7);
	for (var i = 0; i < players.length; i++) {
		var p = players[i];
		for (var a = 0; a < p.canDos.length; a++) {
			var can = p.canDos[a];
			for (var c = 0;c<p.canDos[a].doneIts.length;c++){
				var di = p.canDos[a].doneIts[c];
				if (di.date.between(start, today)) {
					var o = {
						description: p.name + ' - ' + can.description
						, dateText: di.date.equals(today) ? 'Today' : di.date.equals(Date.yesterday()) ? 'Yesterday' : di.date.toFormat('DDD, MMM DD')
						, date: di.date
					}
					result.push(o);
				}
			}
		};
	};

	result.sort(function(a,b){
		return a.date.equals(b.date) ? 0 : a.date.isBefore(b.date) ? 1 : -1;
	})
	return result;
}



router.get('/:id',utils.ensureAuthenticated, function(req,res){
	var id = req.params.id
		, userId = req.user.id;

	var doneItDates = [];
	var today = Date.today();
	for (var i = 0 ; i < 7; i++) {
		var dt = today.clone().addDays(i*-1);
		doneItDates.push({text:dt.toFormat('DDD, MMM D'), value: dt.toFormat('MM/DD/YYYY')})
	};
	var activity = [];
	db.challenge.findOne({'id':id}, function(er, data){
		//TODO handle err
		if (data == null)
			res.send(404);
		else {
			// check if user is in challenge
			var player = data.players.filter(function(p){
				return p.userId == userId;
			}).pop();
			if (!player){
				res.send(404).end();
			}
			else {
				db.userChallenges(userId, function(er, userChallenges){
					//TODO handle error

					db.invite.find({challengeId: data.id}).populate('invitedBy').exec(function(er, invites){
						data.players = currentDoneIts(processor.maxPossible(data.startDate, data.endDate, data.players));

						data.daysRemaining = today.getDaysBetween(data.endDate);
						if (today.isBefore(data.startDate))
							data.daysRemaining = today.getDaysBetween(data.startDate);
						data.weeksRemaining = Math.floor(data.daysRemaining / 7);

						var activity = recentActivity(data.players);

						
						res.render('challenge',{title:data.name, user:req.user, userChallenges:userChallenges, challenge:data, player:player, doneItDates:doneItDates, activity:activity, invites:invites});
					})
						
					
				})
					
			}
				
		}

	});
});



router.get('/:id/calendar',utils.ensureAuthenticated, function(req,res){
	var id = req.params.id
		, userId = req.user.id;

	db.challenge.findOne({'id':id}, function(er, data){
		//TODO handle err
		if (data == null)
			res.send(404);
		else {
			// check if user is in challenge
			var player = data.players.filter(function(p){
				return p.userId == userId;
			}).pop();

			if (player)
				res.render('challenge-calendar',{title:data.name, user:req.user, challenge:data, player:player});
			else
				res.send(404);
		}
	});

});


module.exports = router;
