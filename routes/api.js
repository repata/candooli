var express = require('express');
var router = express.Router();

var db = require('../models/data');
var utils = require('../lib/utils');
var shortid = require('shortid');
var processor = require('../lib/processor');
var settings = require('../settings');

require('date-utils');
/*

	CHALLENGE

*/

/* listing for user. */
router.get('/challenge', utils.ensureAuthenticated, function(req, res) {
  	var user = req.user;
  	if (!user)
  		res.send(402);
  	else {
  		db.challenge.find({'players.userId':user.id}, function(er, data){
  			res.send(data);
  		});
  	}
});

// new challenge
router.post('/challenge', utils.ensureAuthenticated, function(req, res) {
	console.log('creating new challenge');
	var success = true
		, msg = '';

	var name = req.body.name
		, start = req.body.start
		, startDate = new Date(start)
		, durationWeeks = parseInt(req.body.duration)
		, invites = req.body.invites || []
		, canDos = req.body.canDos
		, type = 'Personal'
		, challengeId = shortid.generate();

	//TODO validate invites

	if (invites.length == 1){
		type = '1 on 1'
	}
	else if (invites.length > 1)
		type = 'Group';

	var endDate = startDate.clone().addWeeks(durationWeeks);
	console.log(name);
	console.log(start);
	console.log(invites);


	var ch = new db.challenge();

	ch.id = challengeId;
	ch.name = name;
	ch.startDate = startDate;
	ch.endDate = endDate;
	ch.type = type;
	ch.ownerUserId = req.user.id;
	ch.players = [];
	ch.players.push({
		id:shortid.generate()
		, userId: req.user.id 
		, name: req.user.name
		, canDos:[]
	});

	ch.status = 'Pending';

	for (var i = 0; i < canDos.length; i++) {
		canDos[i].id = shortid.generate();
		canDos[i].doneIts = [];
		ch.players[0].canDos.push(canDos[i]);
	};

	function finish(){
		res.send({success:success, message:msg});
	}

	ch.save(function(er){
		if (!er) {
			processor.challenge(ch, function(er){
				//TODO do something with this
			})
			var tinvites = invites.slice(0);

			// now do invites
			function doInvite(){
				var invite = tinvites.shift();
				if (invite && (invite.email || invite.mobile)) {
					
					var inv = new db.invite();
					inv.id = shortid.generate();
					inv.challengeId = challengeId;
					inv.invitedByUserId = req.user.id;
					inv.email = invite.email || null;
					inv.mobile = invite.mobile || null;
					inv.challenge = ch._id;
					inv.invitedBy = req.user._id;
					inv.invited = null;
					//TODO lookup user being invited
					inv.save(function(er){
						if (er) console.log('invite save error: ' + er);
						if (invite.email){
							// send email
							utils.sendInviteEmail(invite.email, inv.id, null, req.user, function(er){
								//TODO handle errors
							})
						}
						if (invite.mobile){
							utils.sendInviteSms(invite.mobile, inv.id, null, req.user, function(er){
								//TODO handle errors
							})
						}
						doInvite();
					});
				}
				else {
					finish();
				}
			}

			if (tinvites.length > 0){
				doInvite();
				console.log('doing invites');
			}
			else 
				finish();
		}
		else {
			success = false;
			msg = 'Error saving challenge: ' + er;
			finish();
		}
	});

	


});

// edit challenge
router.put('/challenge/:id', utils.ensureAuthenticated, function(req,res){
	var id = req.params.id
		, name = req.body.name
		, start = req.body.start 
		, startDate = null
		, duration = parseInt(req.body.duration)
		, isValid = true;

	var result = {
		success:false
		, message:'Invalid input'
	}

	function finish(){
		res.send(result).end();
	}

	try {
		startDate = new Date(start);
	}
	catch (ex){
		result.message = 'Invalid start date.';
		isValid = false;
		finish();

	}

	if (duration >99 && duration < 1){
		isValid = false;
		result.message = 'Invalid duration, must be between 1 and 99 weeks'
	}
	//TODO validate input
	//TODO encode input to store

	if (startDate !== null){
		db.challenge.findOne({id:id, ownerUserId:req.user.id}, function(er, data){
			if (er){
				result.message = 'Error retrieving database record: ' + er;
				finish();
			}
			else if (data == null){
				result.message = 'Invalid challenge or you are not authorized to make changes to this challenge.';
				finish();
			}
			else {
				data.name = name;
				data.startDate = startDate;
				data.endDate = startDate.clone().addWeeks(duration);
				data.save(function(er){
					if (!er){
						processor.challenge(data, function(er){
							result.success = true;
							result.message = 'ok';
							finish();
						})
					}
					else {
						result.message = 'Error saving challenge: ' + er;
						finish();
					}
				})
			}
		})
	}
		
});

router.get('/challenge/:id', function(req,res){
	var id = req.params.id;

	res.send(id);
});

router.get('/challenge/:id/doneits', utils.ensureAuthenticated, function(req,res){
	var challengeId = req.params.id;
	function doneit(){
		return {
			candoName:''
			, color:''
			, date:null 
			, id:null
			, candoId:null 
			, playerName:null
		}
	}
	var colors = [
		'#F16745', '#FFC65D', '#7BC8A4', '#4CC3D9', '#93648D', '#692DAC', '#9D538E', '#34ACAF'
		//TODO update colors
		, '#F16745', '#FFC65D', '#7BC8A4', '#4CC3D9', '#93648D', '#692DAC', '#9D538E', '#34ACAF'
		, '#F16745', '#FFC65D', '#7BC8A4', '#4CC3D9', '#93648D', '#692DAC', '#9D538E', '#34ACAF'
		, '#F16745', '#FFC65D', '#7BC8A4', '#4CC3D9', '#93648D', '#692DAC', '#9D538E', '#34ACAF'
		, '#F16745', '#FFC65D', '#7BC8A4', '#4CC3D9', '#93648D', '#692DAC', '#9D538E', '#34ACAF'
		, '#F16745', '#FFC65D', '#7BC8A4', '#4CC3D9', '#93648D', '#692DAC', '#9D538E', '#34ACAF'
		, '#F16745', '#FFC65D', '#7BC8A4', '#4CC3D9', '#93648D', '#692DAC', '#9D538E', '#34ACAF'

	];
	db.challenge.findOne({id:challengeId}, function(er, challenge){
		if (er) res.send(500).end();
		else if (challenge == null) res.send(404).end();
		else {
			//authorize user
			var player = challenge.players.filter(function(player){
				return player.userId == req.user.id;
			}).pop();

			if (player) {
				var doneits = [];
				var sdi = new doneit();
				sdi.id = 'challengestart';
				sdi.color = 'silver';
				sdi.title = 'Challenge Started';
				sdi.date = challenge.startDate;
				sdi.candoid = 'X';
				sdi.playerName = 'Challenge';
				sdi.allDay = true;
				doneits.push(sdi);
				for (var p = 0; p < challenge.players.length; p++) {
					var pl = challenge.players[p]
					for (var c = 0; c < pl.canDos.length; c++) {
						var can = pl.canDos[c];
						for (var d = 0; d < can.doneIts.length; d++) {
							var di = can.doneIts[d];

							var dit = new doneit();
							dit.title = can.description;
							dit.color= colors[p];
							dit.date = di.date;
							dit.id = di.id;
							dit.candoId = can.id;
							dit.playerName = p.name;
							dit.allDay = true;
							doneits.push(dit);
						};
					};
				};

				var edi = new doneit();
				edi.id = 'challengeend';
				edi.color = 'silver';
				edi.title = 'Challenge Ends';
				edi.date = challenge.endDate;
				edi.candoid = 'X';
				edi.playerName = 'Challenge';
				edi.allDay = true;
				doneits.push(edi);
				res.send(doneits).end();
			}
			else {
				res.send(401).end();
			}
		}
	})
})

router.post('/challenge/:id/invite', function(req,res){
	var challengeId = req.params.id;
	var userId = req.user.id;
	var result = { success:false, message:''}
	var invite = {};
	invite.email = req.body.email || null;
	invite.mobile = req.body.mobile || null;

	//TODO validate user is in challenge

	function finish(){
		res.send(result).end();
	}

	if (invite != null){
		var email = null;
		if (utils.validateEmail(invite.email))
			email = invite.email;
		var mobile = null;
		if (invite.mobile){
			var ms = invite.mobile.replace(/[^0-9]+/g,'');
			if (s.length == 10 || (s.length == 11 && s.substring(0,1) == '1')){
				mobile = s;
			}

		}
		if (email || mobile){
			//TODO send email or SMS for invite
			db.challenge.findOne({id:challengeId}, function(er, challenge){
				//TODO handle error or invalid challenge
				var inv = new db.invite();
				inv.id = shortid.generate();
				inv.challengeId = challengeId;
				inv.invitedByUserId = req.user.id;
				inv.email = email || null;
				inv.mobile = mobile || null;
				inv.invitedBy = req.user._id;
				inv.invited = null;
				inv.challenge = challenge._id;
				//TODO look up email/mobile for existing user
				//TODO look up if email / mobile already exist in challenge or invites
				inv.save(function(er){
					if (er){
						result.message = 'Error: ' + er;
					} 
					else {

						if (email){
							// send email
							utils.sendInviteEmail(email, inv.id, null, req.user, function(er){
								//TODO handle errors
							})
						}
						if (mobile){
							utils.sendInviteSms(mobile, inv.id, null, req.user, function(er){
								//TODO handle errors
							})
						}

						result.success = true;
					}
					finish();
				});
			})
				
		}
		else {
			result.message = 'Not a valid email or mobile phone number.';
		}
		
	}
	else {
		result.message = 'Invalid data.'
		finish();
	}

	// // now do invites
	// function doInvite(){
	// 	var invite = tinvites.shift();
	// 	if (invite) {
	// 		var inv = new db.invite();
	// 		inv.id = shortid.generate();
	// 		inv.challengeId = challengeId;
	// 		inv.invitedByUserId = req.user.id;
	// 		inv.email = invite.email || null;
	// 		inv.mobile = invite.mobile || null;
	// 		inv.save(function(er){
	// 			if (er) console.log('invite save error: ' + er);
	// 			doInvite();
	// 		});
	// 	}
	// 	else {
	// 		finish();
	// 	}
	// }

	// if (tinvites.length > 0){
	// 	doInvite();
	// }
	// else {
	// 	finish();
	// }
})


/*

	CanDos

*/

/* listing for user. */
router.get('/challenge/:id/cando', utils.ensureAuthenticated, function(req, res) {
  	var userId = req.user;
  	if (!userId)
  		res.send(402);
  	else {
  		db.challenge.find({'players.userId':userId}, function(er, data){
  			res.send(data);
  		});
  	}
});

// new cando
router.post('/challenge/:id/cando', utils.ensureAuthenticated, function(req, res) {

	var challengeId = req.params.id
		, d = req.body.description
		, f = parseInt(req.body.frequency)
		, ft = req.body.frequencyType
		, m = parseInt(req.body.minutes);

	db.challenge.findOne({id:challengeId}, function(er, data){
		if (er) {
			console.log('error: ' + er);
			res.send(500).end();
		}
		else {
			var player = data.players.filter(function(p){
				return p.userId == req.user.id;
			}).pop();

			if (player){
				var candoId = shortid.generate();
				player.canDos.push({
					id: candoId
					, description:d
					, frequency: f 
					, frequencyType: ft 
					, minutes: m 
					, doneIts:[]
					, totalCount:0
					, doneItCount:0
				});

				data.save(function(er){
					processor.challenge(data, function(er){
						//TODO handle er
						res.send({success:!er, message:er || '', newId:candoId}).end();
					})
				})
			}
			else {
				res.send(401).end();
			}
		}
	})


});



// edit cando
router.put('/challenge/:id/cando/:candoid', utils.ensureAuthenticated, function(req,res){

	var challengeId = req.params.id
		, candoId = req.params.candoid
		, d = req.body.description
		, f = parseInt(req.body.frequency)
		, ft = req.body.frequencyType
		, m = parseInt(req.body.minutes);

	db.challenge.findOne({id:challengeId}, function(er, data){
		if (er) {
			console.log('error: ' + er);
			res.send(500).end();
		}
		else {
			var player = data.players.filter(function(p){
				return p.userId == req.user.id;
			}).pop();

			if (player){
				var cando = player.canDos.filter(function(can){
					return can.id == candoId;
				}).pop();

				if (cando){
					cando.description = d;
					cando.frequency = f;
					cando.frequencyType = ft;
					cando.minutes = m;


					data.save(function(er){
						processor.challenge(data, function(er){
							//TODO handle er
							res.send({success:!er, message:er || ''}).end();
						})
					})
				}
				else {
					res.send(404).end();
				}

			}
			else {
				res.send(401).end();
			}
		}
	})

});

// delete cando
router.delete('/challenge/:id/cando/:candoid', utils.ensureAuthenticated, function(req,res){
	var challengeId = req.params.id
		, candoId = req.params.candoid;

	db.challenge.findOne({id:challengeId}, function(er, data){
		if (er) {
			console.log('error: ' + er);
			res.send(500).end();
		}
		else {
			var player = data.players.filter(function(p){
				return p.userId == req.user.id;
			}).pop();

			if (player){
				var ar = [];
				for (var i = 0; i < player.canDos.length; i++) {
					if (player.canDos[i].id != candoId){
						ar.push(player.canDos[i]);
					}
				};
				
				player.canDos = ar;

				data.save(function(er){
					processor.challenge(data, function(er){
						//TODO handle er
						res.send({success:!er, message:er || ''}).end();
					})
				})
			}
			else {
				res.send(401).end();
			}
		}
	})

})

router.get('/challenge/{id}/cando/{candoid}', utils.ensureAuthenticated, function(req,res){
	var id = req.params.id;

	res.send(id);
});


// add doneit
router.post('/challenge/:id/cando/:candoid/doneit', utils.ensureAuthenticated, function(req,res){

	var challengeId = req.params.id 
		, canDoId = req.params.candoid
		, userId = req.user.id;

	var date = new Date(req.body.date)
		, minutes = parseInt(req.body.minutes)
		, note = req.body.note;

	//TODO validate

	db.challenge.findOne({id:challengeId}, function(er, challenge){
		if (challenge == null) res.send(404).end();
		else {
			var player = challenge.players.filter(function(p){
				return p.userId == userId;
			}).pop();

			if (!player) res.send(404).end();
			else {
				var cando = player.canDos.filter(function(c){
					return c.id == canDoId;
				}).pop();

				if (!cando) res.send(404).end();
				else {
					cando.doneIts.push({id:shortid.generate(), date:date, minutes:minutes, note:note});
					processor.challenge(challenge, function(er){
						var result = {
							success:true
							, players:[]
							, currentCanDoCount:0
						};
						for (var i = 0; i < challenge.players.length; i++) {
							var pl = challenge.players[i];

							result.players.push({
								id:pl.id
								, userId:pl.userId
								, totalPercent: pl.totalPercent
								, rank:pl.rank
							});
						};
						res.send(result).end();
					})
					// challenge.save(function(er){
					// 	res.send({success:true}).end();
					// })
				}
			}
			// for (var i = 0; i < challenge.players.length; i++) {

			// 	var p = challenge.players[i]
			// };
		}
	})

})


/*

	DoneIts

*/

/* listing for user. */
router.get('/challenge/{id}/doneit', function(req, res) {
  	var userId = req.user;
  	if (!userId)
  		res.send(402);
  	else {
  		db.challenge.find({'players.userId':userId}, function(er, data){
  			res.send(data);
  		});
  	}
});

// new challenge
router.post('/challenge/{id}/doneit', function(req, res) {
  res.send('not implemented');
});

// edit challenge
router.put('/challenge/{id}/doneit/{doneitid}', function(req,res){
	var id = req.params.id;

	res.send(id);
});

router.get('/challenge/{id}/doneit/{doneitid}', function(req,res){
	var id = req.params.id;
	res.send(id);
});




/*

	USERS

*/

//edit  profile
router.put('/user/:id', utils.ensureAuthenticated, function(req,res){
	var name = req.body.name
		, email = req.body.email
		, mobile = req.body.mobile;

	req.user.name = name;
	req.user.save(function(er){
		console.log('done');
		res.send({success:!er, message: er || ''})
	})
})



module.exports = router;
