var express = require('express');
var router = express.Router();
var utils = require('../lib/utils');
var shortid = require('shortid');


var db = require('../models/data');
/* GET users listing. */
router.get('/', utils.ensureAuthenticated, function(req, res) {
	if (req.user.role == 'admin'){
		db.user.find({}, function(er, users){
			res.render('admin-users.jade', {title:'Users', users:users});
		})
	}
	else {
		res.send(404).end();
	}
		
});


router.get('/:id', utils.ensureAuthenticated, function(req,res){
	db.userChallenges(req.user.id, function(er, data){
		res.render('user', {title:'Profile', userChallenges:data, user:req.user})
	})
})

//edit  profile
router.put('/:id', utils.ensureAuthenticated, function(req,res){
	var name = req.body.name
		, email = req.body.email
		, mobile = req.body.mobile
		, password1 = req.body.password
		, password2 = req.body.password2
		, errors = [];

	//TODO finish - sending multi errors and only sending email/mobile verify after user.save

	if (name.length < 4){
		errors.push('Invalid name, must be at least 4 characters');
	}
	else if (name.toLowerCase() != req.user.name.toLowerCase())
		req.user.name = name; {
	}

	var cleanMobile = utils.cleanMobile(mobile);
	

	if (email.toLowerCase() != req.user.email.toLowerCase()) {
		//validate email
		if (utils.validateEmail(email)){
			var emailCode = shortid.generate();
			req.user.emailVerificationCode = emailCode;
			req.user.emailVerified = false;
			req.user.emailVerificationSendDate = new Date();
			req.user.email = email;

			utils.sendEmailVerification(req.user.name, emailCode, function(er){
				//TODO handle error, log to app log for viewing/alertings
			})
		}
	}

	if (cleanMobile != req.user.mobile){
		if (cleanMobile == null){
			req.user.mobile = null;
			req.user.mobileVerificationCode = null;
			req.user.mobileVerified = false;
		}
		else {
			var mobileCode = utils.randomInt(1000,9999);
			req.user.mobile = cleanMobile;
			req.user.mobileVerificationCode = mobileCode;
			req.user.mobileVerificationSendDate = new Date();
			req.user.mobileVerified = false;

			utils.sendMobileVerification(cleanMobile, mobileCode, function(er){
				//TODO handle error
			})
		}
	}

	
	req.user.save(function(er){
		console.log('done');
		db.userChallenges(req.user.id, function(er, data){
			res.render('user', {title:'Profile', userChallenges:data, user:req.user, errors:errors})
		})
	})
})


module.exports = router;
