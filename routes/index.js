var express = require('express');
var router = express.Router();
var passport = require('passport');
var db = require('../models/data');
var shortid = require('shortid');
var utils = require('../lib/utils');

/* GET home page. */
router.get('/', function(req, res) {

	if (req.user){
		var user = req.user;
		var userId = user.id;
		console.log(req.user);
		console.log('userid: ' + userId);
		db.getUserData(userId, function(er, challenges){
			db.invite.find({email:req.user.email, status:'Open'}).populate('challenge invitedBy').exec(function(er, invites){
				//TODO handle error
				res.render('index', { title: 'Candooli', userChallenges:challenges, user:req.user, invites:invites });	
			})

		});
		// db.challenge.find({'players.userId':userId}, function(er, challenges){
		// 	//TODO also do mobiles
		// 	for (var i = 0; i < challenges.length; i++) {
		// 		for (var a = 0; a < challenges[i].players.length; a++) {
		// 			if (challenges[i].players[a].userId == user.id){
		// 				challenges[i].userIx = a;
		// 				break;
		// 			}
		// 		};
				
		// 	};
		// 	//TODO get invites for mobile too
		// 	db.invite.find({email:req.user.email, status:'Open'}).populate('challenge invitedBy').exec(function(er, invites){
		// 		//TODO handle error
		// 		res.render('index', { title: 'Candooli', userChallenges:challenges, user:req.user, invites:invites });	
		// 	})
			
		// });
	} else {
		res.render('index', { title: 'Candooli', userChallenges:[], user:null });
	}
	

});

// verify mobile 
router.get('/verifyMobile', utils.ensureAuthenticated, function(req, res){
	res.render('verify-mobile', {title:'Verify Your Mobile Phone Number', user:req.user});
});

router.post('/verifyMobile', utils.ensureAuthenticated, function(req, res){

	var code = req.body.code.trim();

	db.user.findOne({id:req.user.id}, function(er, data){
		if (data != null){
			//TODO check date of code and if it hasn't already been verified

			if (data.mobileVerificationCode == code){
				data.mobileVerified = true;
				data.mobileVerificationCode = null;
				data.save(function(er){
					res.render('verify-mobile', {title:'Verify Your Mobile Phone Number', success:true, user:req.user});
				})
			}
			else {
				res.render('verify-mobile', {title:'Verify Your Mobile Phone Number', user:req.user, message:'Invalid verification code, please check it and try again.'})
			}
		}
		else {
			res.send(404);
		}
	})

	
})

// verify email address
router.get('/v/:verifyId', function(req,res){
	var id = req.params.verifyId;
	var msg = 'Sorry, we could not find that verification code.';
	db.user.findOne({emailVerificationCode:id}, function(er, data){
		if (data != null){
			if (data.emailVerified){
				msg = 'Thanks for checking, but this email address is already verified.';
			}
			else if (new Date(data.emailVerificationSendDate).getDaysBetween(Date.today()) > 3){
				msg = 'This verification code has expired.';
			}
			else {
				msg = 'Email verified!  Thank you and go get stuff done.';
				data.emailVerified = true;
				data.save(function(er){

				});
			}
		}
		

		res.render('verify-email', {title:'Email Verification', message:msg});
	})
})

router.get('/i/:inviteId', function(req,res){
	var user = req.user
		, inviteId = req.params.inviteId
		, invite = null
		, userId = null;
	if (user) {
		userId = user.id;
	} 

	db.invite.findOne({id:inviteId}).populate('invitedBy challenge').exec(function(er, inv){
		if (inv != null) {
			invite = inv;
			//db.user.findOne({id: invite.invitedByUserId}, function(er, invitedBy){
				//TODO handle errors
				//db.challenge.findOne({id: invite.challengeId}, function(er, challenge){
					if (req.user){
						// check if this is the right user
						if (
							(inv.email != null && inv.email == req.user.email)
							|| (inv.mobile != null && inv.mobile == req.user.mobile)
							){
							var player = inv.challenge.players.filter(function(p){
								return p.userId == invite.invitedByUserId;
							}).pop();
							console.log(player.minutesPerWeek);
							//var hoursPerWeek = Math.floor(player.minutesPerWeek / 60 * 10) / 10;
							//console.log(hoursPerWeek);
							//invite.invitedBy.hoursPerWeek = hoursPerWeek;
							//invite.challenge = invite.challenge;
							//invite.invitedBy = inv.invitedBy;
							invite.canDos = player.canDos;
							finish();
						}
						else {
							//res.send(401).end();
							//TODO update message to explain about logging out, etc.
							res.render('message', {title:'Unauthorized', message:'This invite is for someone else.', user:req.user})
						}
					}
					else {
						var player = inv.challenge.players.filter(function(p){
							return p.userId == invite.invitedByUserId;
						}).pop();
						console.log(player.minutesPerWeek);
						//var hoursPerWeek = Math.floor(player.minutesPerWeek / 60 * 10) / 10;
						//console.log(hoursPerWeek);
						//invite.invitedBy.hoursPerWeek = hoursPerWeek;
						//invite.challenge = invite.challenge;
						//invite.invitedBy = inv.invitedBy;
						invite.canDos = player.canDos;
						finish();
					}
					
				//});
			//});
		} 
		else
			res.send(404);

	});

	function finish(){
		if (userId === null)
			res.render('invite',{title:'Invite', user:user, invite:invite, userChallenges:[]});
		else
			db.getUserData(userId, function(er, cs){
				res.render('invite',{title:'Invite', user:user, invite:invite, userChallenges:cs});
			})
	}
	
})


router.get('/i/:inviteId/decline', utils.ensureAuthenticated, function(req,res){
	var inviteId = req.params.inviteId
	db.invite.findOne({id:inviteId}, function(er, invite){
		res.send('not implemented yet.')
	})
});

router.get('/i/:inviteId/accept', utils.ensureAuthenticated, function(req,res){
	var user = req.user
		, inviteId = req.params.inviteId
		, userId = null;
	
	

	db.acceptInvite(inviteId, req.user, function(result){
		if (result.success){
			res.redirect('/challenges/' + result.challengeId);
		}
		else if (result.code == 404)
			res.send(404).end();
		else if (result.code == 500)
			res.send(500).end();
		else if (result.code == 401)
			res.send(401).end();
		else {
			res.send('Unknown problem occurred');
		}
	})

	// db.invite.findOne({id:inviteId}).exec(function(er, invite){
	// 	if (invite != null) {
	// 		db.challenge.findOne({id:invite.challengeId}, function(er, challenge){

	// 			var already = challenge.players.filter(function(p){
	// 				return p.userId == req.user.id;
	// 			}).pop();

	// 			if (already){
	// 				finish(challenge.id);
	// 			}
	// 			else if (invite.email != null && invite.email.toLowerCase() == req.user.email.toLowerCase()){
	// 				invite.status = 'Accepted';
	// 				invite.save(function(er){
	// 					if (er) res.send(500).end();
	// 					else {
	// 						challenge.players.push({
	// 							id:shortid.generate()
	// 							, name: req.user.name
	// 							, userId: req.user.id
	// 							, canDos:[]
	// 							, totalCanDoCount:0
	// 							, totalDoneItCount: 0
	// 							, totalPercent:0
	// 							, minutesPerWeek: 0
	// 							, hoursPerWeek: 0
	// 							, rank:99
	// 						})
	// 						challenge.save(function(er){
	// 							if (er) res.send(500).end();
	// 							else finish(invite.challengeId);
	// 						})
							
	// 					}
	// 				})
	// 			}
	// 			else if (invite.mobile != null && invite.mobile == req.user.mobile){
	// 				invite.status = 'Accepted';
	// 				invite.save(function(er){
	// 					if (er) res.send(500).end();
	// 					else {
	// 						challenge.players.push({
	// 							id:shortid.generate()
	// 							, name: req.user.name
	// 							, userId: req.user.id
	// 							, canDos:[]
	// 							, totalCanDoCount:0
	// 							, totalDoneItCount: 0
	// 							, totalPercent:0
	// 							, minutesPerWeek: 0
	// 							, hoursPerWeek: 0
	// 							, rank:99
	// 						})
	// 						challenge.save(function(er){
	// 							if (er) res.send(500).end();
	// 							else finish(invite.challengeId);
	// 						})
							
	// 					}
	// 				})
	// 			}
	// 			else
	// 				res.send(404).end();
	// 		})
			
			
	// 	} 
	// 	else
	// 		res.send(404).end();
	// });


});

router.get('/test', function(req,res){
	res.render('test',{title:'test'});
});

router.route('/login')
	.get(function(req,res){
		var message = null;
		if (req.query.failed){
			message = "Invalid email / password."
		}
		res.render('login.jade', {title:'Login', message:message});

	})
	//.post(passport.authenticate('local', { successRedirect: '/', failureRedirect: '/login?failed=true' }));
	.post(passport.authenticate('local', {failureRedirect:'/login?failed=true'}), function(req,res){
		if (req.query['inv'] != undefined){
			var inviteId = req.query['inv'];
			//do the invite thing, then 
			db.acceptInvite(inviteId, req.user, function(result){
				if (result.success){
					res.redirect('/challenges/'+result.challengeId);
				}
				else {
					var msg = 'Unknown error';
					switch (result.code){
						case 404:
							msg = 'Invitation not found';break;
						case 401:
							msg = 'Invitation not found';break;
						case 500:
							msg = 'Error: ' + result.message;break;
					}
					res.render('message', {title:'Problems accepting invitation', message:msg});
				}
			})
		}
		else
			res.redirect('/');
	})

router.get('/thank-you', function(req,res){
	res.render('thank-you', {title:'Log Out'})
});

router.get('/logout', function(req,res){
	req.logout();
	res.redirect('/thank-you');
})
/*
router.post('/login', function(req,res, next){
	console.log('logging in')

	//passport.authenticate('local', { successRedirect: '/',
    //                                   failureRedirect: '/login' })

 passport.authenticate('local-login', function(err, user, info) {
	    if (err) { return next(err) }
	    if (!user) {
			console.log('no user')
	      //req.session.messages =  [info.message];
	      //return res.redirect('/login')
	      return res.render('login.jade',{title:'Login', message:'Invalid email and password combination.', version:'1.0'})
	    }
	    req.logIn(user, function(err) {
			console.log('logged in');
	      if (err) { return next(err); }
	      return res.redirect('/');
	    });
	  })(req, res, next);
});
*/

router.get('/register', function(req,res){
	res.render('register', {title:'Register'});
});

router.post('/register', function(req,res){
	var userid = shortid.generate()
		, name = req.body.name
		, email = req.body.username
		, mobile = req.body.mobile
		, pw = req.body.password
		, errors = []
		, doMobileVerify = false
		, inviteId = req.query['inv'] || null;

 	var user = {
 		name:name
 		, email:email 
 		, mobile:mobile 

 	}

 	var cleanMobile = utils.cleanMobile(mobile);

 	if (!name || name.length < 4)
 		errors.push('Your name must be at least 3 characters');

 	if (!utils.validateEmail(email))
 		errors.push('Invalid email address');


	if (cleanMobile){
		if (mobile.length == 11 && mobile.substring(0,1) == 1){

		} else if (mobile.length == 10){
			mobile = '1' + mobile;
		}
		else {
			mobile = null;
			errors.push('Mobile number is not valid.');
		}
	}

	if (mobile && cleanMobile == null){
		errors.push('Invalid mobile number');
	}

 	if (!pw || pw.length < 8)
 		errors.push('Your password must be at least 8 characters');
	
	
	function add(){
		var emailVerificationCode = shortid.generate();
		var mobileVerificationCode = utils.randomInt(1000,9999);
		var user = new db.user();
		user.id = userid;
		user.name = name;
		user.email = email;
		user.mobile = mobile;
		user.mobileVerified = false;
		if (mobile){
			user.mobileVerificationCode = mobileVerificationCode;
			user.mobileVerificationSendDate = new Date();
			doMobileVerify = true;
		}
		user.emailVerificationCode = emailVerificationCode;
		user.emailVerificationSendDate = new Date();
		user.emailVerified = false;
		user.password = pw;
		user.dateCreated = new Date();
		user.dateUpdated = new Date();

		user.role = 'general';
		user.save(function(er){
			if (er) {
				errors.push('Error saving user: ' + er);
				sendErrors();
			}
			else {
				if (doMobileVerify){
					utils.sendMobileVerification(mobile, user.mobileVerificationCode, function(er, d){
						console.log('sent mobile verification: ' + d.toString());
					})
				}

				utils.sendEmailVerification(name, email, user.emailVerificationCode, function(er){
					console.log('sent email verification: ' + er);
				})
				// send email verification
				passport.authenticate('local')(req,res, function(){
					var inv = '';
					if (inviteId) inv = '?inv=' + inviteId; 

					if (doMobileVerify) {
						res.redirect('/verifyMobile' + inv);
					}
					else {
						if (inviteId){
							db.acceptInvite(inviteId, req.user, function(result){
								if (result.success){
									res.redirect('/challenges/' + result.challengeId);
								}
								else {
									//TODO handle problem with invite better than just redirecting
									//res.render('message', {title: })
									res.redirect('/');
								}
							})
						} else
							res.redirect('/');
					}
				});
			}

		})
	}

	function sendErrors(){
		var msg = errors.join('<br/>');
		res.render('register',{title:'Register', message: msg, userObj:user});
	}

	

	if (errors.length == 0){
		db.emailExists(email.toLowerCase(), function(er, exists){
			if (er) errors.push('Error validating email: ' + er);
			if (exists) {
				errors.push('The email address provided is already registered.');
				sendErrors();
			}
			else
				add();
		})
	}
	else {
		errors.push('Invalid email address');
		sendErrors();
	}
	
		
})

module.exports = router;
