var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session    = require('express-session');
var MongoStore = require('connect-mongo')(session);
var favicon = require('serve-favicon');
var schedule = require('node-schedule');

var settings = require('./settings');

//routes
var routes = require('./routes/index');
var users = require('./routes/users');
var challenges = require('./routes/challenges');
var api = require('./routes/api');
var routei = require('./routes/i');


var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy

var db = require('./models/data');

//passport session setup
passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  db.user.findOne({id:id}, function (err, user) {
    done(err, user);
  });
});

//var RedisStore = require('connect-redis')(express);
passport.use(new LocalStrategy(
  function(username, password, done) {
    db.user.findOne({ email: username }, function (err, user) {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      user.comparePassword(password, function(err, isMatch) {
        if (err) return done(err);
        if(isMatch) {
            return done(null, user);
        } else {
            return done(null, false, { message: 'Invalid password' });
        }
        });
      
    });
  }
));

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(__dirname + '/public/images/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(session({
    secret: 'hivixopamokevahasuditavisanubeyi12642619',
    store: new MongoStore({
      url : settings.db,
    })
  }));
//app.use(session({secret: 'hivixopamokevahasuditavisanubeyi12642619'}))

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.use('/challenges', challenges);
app.use('/api', api);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
//if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
//}

// production error handler
// no stacktraces leaked to user
// app.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     res.render('error', {
//         message: err.message,
//         error: {}
//     });
// });


module.exports = app;



var rule = new schedule.RecurrenceRule();
  rule.hour = 3;
  rule.minute = [5];

var j = schedule.scheduleJob(rule, function(){
    var today = Date.today();

    db.challenge.update({startDate:today}, {$set:{status:'In progress'}}, {multi:true}, function(er){
      console.log('set challenges starting today to inprogress: ' + er);
    })

});

var today = Date.today();
var yesterday = Date.yesterday();
    db.challenge.update({startDate:today}, {$set:{status:'In progress'}}, {multi:true}, function(er){
      console.log('set challenges starting today to inprogress: ' + er);
    });

    db.challenge.update({endDate:yesterday}, {$set:{status:'Completed'}}, {multi:true}, function(er){
      console.log('set challenges starting today to inprogress: ' + er);
    });


