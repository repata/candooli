

var settings = require('../settings')
    , email = require('emailjs')
    , request = require('request');

var smtp  = email.server.connect({
   user:    settings.email.username, 
   password:settings.email.password, 
   host:    settings.email.smtpServer, 
   ssl:     true
});



module.exports.ensureAuthenticated = function (req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login');
}

module.exports.validateEmail = function (email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

module.exports.cleanMobile = function(mobile){
  var s = null;
  if (mobile){
    s = mobile.toString().replace(/[^0-9\.]+/g,'');
    if (s.length < 10){
      s = null;
    }
    else if (s.length == 10) {
      s = '1' + s;
    }
    else if (s.length == 11 && s.substring(0,1) != '1'){
      s = null;
    }
  }
    
  return s;
}

module.exports.randomInt = function(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}


module.exports.sendEmail = sendEmail;

function sendEmail(to, subject, body, callback){
// send the message and get a callback with an error or details of the message that was sent
    smtp.send({
       text:    body, 
       from:    "Candooli Support <" + settings.email.from + ">", 
       to:      to,
       subject: subject
    }, function(err, message) { 
        if (callback){
            callback(err);
        } else if (err){
            console.log('EMAIL SEND ERROR: ' + err);
        }
     });

}


module.exports.sendEmailWithAttachments = function (to, subject, body, attachments, callback){
// send the message and get a callback with an error or details of the message that was sent
    smtp.send({
       text:    body, 
       from:    "Candooli Support <" + settings.email.from + ">", 
       to:      to,
       subject: subject,
       attachment:attachments
    }, function(err, message) { 
        if (callback){
            callback(err);
        } else if (err){
            console.log('EMAIL SEND ERROR: ' + err);
        }
     });

}


function sendSms(to, msg, callback) {
    // https://api.clickatell.com/http/sendmsg?user=repata&password=pass@123&api_id=3162028&to=&text=

    var url = 'https://api.clickatell.com/http/sendmsg?user=repata&password=pass@123&api_id=3162028'

    url += '&to=' + to;
    url += '&text=' + msg;

    request(url, function(er, response, body){
        //TODO handle stuff

        var result = body;
        if (callback)
            callback(er, result);
    })
}


module.exports.sendSms = sendSms;

module.exports.sendMobileVerification = function(mobile, code, callback){
	var msg = 'Your Candooli mobile verification code is ' + code;
	sendSms(mobile, msg, callback);
}


//TODO create queue process to submit emails and sms to
module.exports.sendEmailVerification = function(name, email, code, callback){
	var msg = "Hi " + name + ",\n Please verify your email address by clicking on the link below.\n\n" 
	+ settings.baseUrl + '/v/' + code
	+ '\n\nThanks,\n\nCandooli Team';
	sendEmail(name + '<' + email + '>', 'Candooli.com Email verification.', msg, callback);
}



module.exports.sendInviteEmail = function (email, inviteId, challenge, user, callback){
  var body = 'Hi ' + email + ',\n\n' + user.name + ' has challenged you to get things done via Candooli.com.\n\n'
    + 'Go to ' + settings.baseUrl + '/i/' + inviteId + ' to check in out.\n\nHave a great day,\n\nCandooli Team';
//TODO convert this to queue based.
  sendEmail(email, user.name + ' has challenged you', body, function(er){
    console.log('Invite email sent: ' + er);
    callback(er);
  });
}

module.exports.sendInviteSms = function(mobile, inviteId, challenge, user, callback){
  var body = user.name + ' has challenged you on Candooli. Go to ' + settings.baseUrl + '/i/' + inviteId + ' to check it out!';
  //TODO convert this to queue based.
  sendSms(mobile, body, callback);
}

