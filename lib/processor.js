require('date-utils');


function processPlayer(player, callback){}

function processChallenge(challenge, callback){
/*
	
*/
	var challengeStart = new Date(challenge.startDate.getTime());

	var today = Date.today()
		, totalDays = challengeStart.getDaysBetween(challenge.endDate)//challenge.startDate.getDaysBetween(challenge.endDate)
		, daysLeft = today.getDaysBetween(challenge.endDate)
		//, firstSunday = challenge.startDate.getDay;

	console.log('Challenge: ' + challenge.name);
	console.log('total days: ' + totalDays);
	console.log('Start: ' + challenge.startDate);
	console.log('End: ' + challenge.endDate);

	for (var p = 0; p < challenge.players.length; p++) {
		var player = challenge.players[p];
		
		console.log(player.name);
		console.log('Can Dos: ' + player.canDos.length);
		var totalCanDoCount = 0;
		var totalDoneItCount = 0;
		var minutesPerWeek = 0;
		var hoursPerWeek = 0;
		// loop though candos
		for (var c = 0; c < player.canDos.length; c++) {
			var cando = player.canDos[c];
			var timePeriods = totalDays;

			console.log('CanDo: ' + cando.description + ' ' + cando.frequency + ' times per ' + cando.frequencyType);
			console.log('Done Its: ' + cando.doneIts.length);

			switch (cando.frequencyType.toLowerCase()){
				case 'one time':
					timePeriods =1;
					break;
				case 'day':
					timePeriods = totalDays;
					minutesPerWeek+= cando.frequency * cando.minutes * 7;
					break;
				case 'week':
					timePeriods = Math.ceil(totalDays / 7);
					minutesPerWeek+= cando.frequency * cando.minutes;
					break;
				case 'month':
					timePeriods = Math.ceil(totalDays / 28);
					break;
			}
			var candoCount = cando.frequency * timePeriods;

			totalCanDoCount+= candoCount;
			var candoDoneIts = 0;

			for (var i=0;i<timePeriods;i++){
				var day = challenge.startDate.clone().addDays(i)
					, weekStart = challenge.startDate.clone().addWeeks(i)
					, weekEnd = weekStart.clone().addDays(6);
				//console.log('week: ' + weekStart + ' - ' + weekEnd);

				var dones = cando.doneIts.filter(function (doneit) {
				    
				    if (cando.frequencyType.toLowerCase() == 'one time'){
				    	return true;
					}
					else if (cando.frequencyType.toLowerCase() == 'day'){
						return doneit.date.equals(day);
					}
					else if (cando.frequencyType.toLowerCase() == 'week'){
						return doneit.date.between(weekStart, weekEnd);
					}
				});

				//console.log('filter results: ' + dones.length);
				candoDoneIts+= (dones.length > cando.frequency ? cando.frequency : dones.length);

				//console.log('candoDoneIts = ' + candoDoneIts)
			}

			totalDoneItCount+= candoDoneIts;
				//console.log('totalDoneItCount = ' + totalDoneItCount);
			challenge.players[p].canDos[c].doneItCount = candoDoneIts;
			challenge.players[p].canDos[c].totalCount = candoCount;
			

			console.log(cando.description + '. Cando Count: ' + candoCount + ', DoneIts: ' + candoDoneIts);

		}; // loop candos

		hoursPerWeek = Math.floor(minutesPerWeek / 60 * 10) / 10;

		challenge.players[p].minutesPerWeek = minutesPerWeek;
		challenge.players[p].hoursPerWeek = hoursPerWeek;
		challenge.players[p].totalCanDoCount = totalCanDoCount;
		challenge.players[p].totalDoneItCount = totalDoneItCount;
		challenge.players[p].totalPercent = totalCanDoCount > 0 ? (totalDoneItCount / totalCanDoCount) : 0;

	};

	// do ranking

	challenge.players.sort(function(a,b){
		if (a.totalPercent == b.totalPercent)
			return a.totalCanDoCount > b.totalCanDoCount ? -1 : 1;
		return a.totalPercent > b.totalPercent ? -1 : 1;
	});

	for (var i = 0; i < challenge.players.length; i++) {
		challenge.players[i].rank = i+1;
	};
	challenge.save(function(er){
		callback(er);
	});
	

}

function calcMaxPossible(startDate, endDate, players){

	var today = Date.today()
		, daysLeft = today.getDaysBetween(endDate)
		, weeksLeft = Math.ceil(daysLeft / 7);
	console.log('today: ' + today);
	console.log('daysleft: ' + daysLeft);
	console.log('weeksleft: ' + weeksLeft);

	for (var i = 0; i < players.length; i++) {
		var p = players[i]
			, n = p.totalPercent
			, maxCanDosLeft = 0
			, current = p.totalDoneItCount;

		if (today.isBefore(startDate))
			players[i].maxPossiblePercent = 1;
		else
		{
			for (var a = 0; a < p.canDos.length; a++) {
				switch (p.canDos[a].frequencyType.toLowerCase()){
					//TODO add in one timers
					case "day":
						maxCanDosLeft+= (p.canDos[a].frequency * daysLeft);
						break;
					case "week":
						maxCanDosLeft+= (p.canDos[a].frequency * weeksLeft);
						break;
				}
			};
			console.log('candos: ' + p.totalCanDoCount + ', left: ' + (maxCanDosLeft + current));
			n = p.totalCanDoCount > 0 ? ((maxCanDosLeft + current) / p.totalCanDoCount) : 0;

			players[i].maxPossiblePercent = n > 1 ? 1 : n;
		}
		
	};

	return players;
}


module.exports = {
	challenge:processChallenge
	, maxPossible:calcMaxPossible
}