const Schema = require('mongoose').Schema
    , ObjectId = Schema.ObjectId;



var Challenge = module.exports = new Schema({
	id: {type:String, required:true}
	, name: { type: String, required: true }
	, cancelled: {type:Boolean, default:false}
	, status: {type:String, required:true } // Upcoming, In Progress, Completed
	, startDate: {type:Date, required:true}
	, endDate: {type:Date} 
	, type: { type:String, required:true} // 1 on 1, Group
	, ownerUserId: {type:String, required:true}
	, dateCreated:{ type: Date, default: Date.now }
	, dateUpdated:{ type: Date, default: Date.now }
	, players:[
		{
			id: {type:String, required:true}
			, userId: {type:String, required:true}
			, name: {type:String, required:true}
			, totalCanDoCount: {type:Number, default:0}
			, totalDoneItCount: {type:Number, default:0}
			, totalPercent: {type: Number, default:0}
			, minutesPerWeek: {type:Number, default:0 }
			, hoursPerWeek: {type:Number, default:0 }
			, rank: {type:Number, default:1}
			, canDos:[
				{
					id: {type:String, required:true}
					, description: {type:String, required:true}
					, frequency: {type: Number, default:1, required:true}
					, frequencyType: {type:String, required:true} // One Time, Day, Week, Month, Quarter, Year
					, minutes: {type: Number, default:1, required:true}
					, doneItCount: {type:Number, default:0}
					, totalCount: {type:Number, default:0}
					, doneIts:[
						{
							id: {type:String, required:true}
							, date: {type:Date, required:true}
							, minutes: Number
							, note: String
						}
					]
				}
			]
		}
	]
});





// var challenge = {
// 	id:''
// 	, name:''
// 	, type:'' // 1 on 1, Group
// 	, ownerUserId:''
// 	, players:[
// 		{
// 			id:''
// 			, name:''
// 			, canDos:[
// 				{
// 					id:''
// 					, description:''
// 					, frequency:0
// 					, frequencyType:'' // One Time, Day, Week, Month, Quarter, Year
// 					, minutes:0
// 					, doneIts:[
// 						{
// 							id:''
// 							, date:''
// 							, minutes:0
// 							, note:''
// 						}
// 					]
// 				}
// 			]
// 		}
// 	]
// }