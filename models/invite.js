const Schema = require('mongoose').Schema
    , ObjectId = Schema.ObjectId;

/**
 * Schema.
 */

var Invite = module.exports = new Schema({
	id: {type:String, required:true}
	, challengeId: {type:String, required:true}
	, invitedByUserId: {type:String, required:true}
	, email: {type:String, default:null}
	, mobile: {type:String, default:null}
	, dateInvited: {type:Date, default: Date.now}
	, status: {type:String, default: 'Open'}
	, invitedBy:{type:ObjectId, ref:'User'}
	, invited: {type: ObjectId, ref:'User'}
	, challenge: {type:ObjectId, ref:'Challenge'}
});

Invite.index({ id: 1, challengeId: 1, invitedByUserId:1 }); // schema level