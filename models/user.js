var crypto = require('crypto');

const Schema = require('mongoose').Schema
    , ObjectId = Schema.ObjectId;

/**
 * Schema.
 */

var User = module.exports = new Schema({
    id:{type:String, required:true}
    , name:{type:String, required:true}
    , email:{type:String, required:true}
    , mobile:{type:String, default:null}
    , password:String
    , role:{type:String, default:'general'} // admin, basic
    , emailVerificationCode:{type:String, default:null}
    , emailVerified: {type:Boolean, default:false}
    , emailVerificationSendDate: {type:Date, default:null}
    , mobileVerificationCode:{type:String, default:null}
    , mobileVerified: {type:Boolean, default:false}
    , mobileVerificationSendDate: {type:Date, default:null}
    , dateCreated: {type:Date, required:true}
    , dateUpdated: {type:Date, required:true}

});

var salt = 'yafufupihexubinipajovilu10449335suhadokazihafubodasusuva93927961';
// Bcrypt middleware
User.pre('save', function(next) {
    var user = this;

    if(!user.isModified('password')) return next();

    var epassword = crypto.createHmac('sha1', salt).update(user.password).digest('hex');
    user.password = epassword;
    next();
    
});

// Password verification
User.methods.comparePassword = function(candidatePassword, cb) {
    var epassword = crypto.createHmac('sha1', salt).update(candidatePassword).digest('hex');
    cb(null, epassword == this.password);
    
};