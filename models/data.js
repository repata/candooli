
const mongoose = require('mongoose');

var settings = require('../settings')
	, shortid = require('shortid')
	, processor = require('../lib/processor');

require('date-utils');

require('./config')();
const db = mongoose.connect(settings.db);

mongoose.connection.on('connected', function(er){
	console.log('Database connected.');
});

var colors = [
	'#F16745', '#FFC65D', '#7BC8A4', '#4CC3D9', '#93648D', '#692DAC', '#9D538E', '#34ACAF'
	//TODO update colors
	, '#F16745', '#FFC65D', '#7BC8A4', '#4CC3D9', '#93648D', '#692DAC', '#9D538E', '#34ACAF'
	, '#F16745', '#FFC65D', '#7BC8A4', '#4CC3D9', '#93648D', '#692DAC', '#9D538E', '#34ACAF'
	, '#F16745', '#FFC65D', '#7BC8A4', '#4CC3D9', '#93648D', '#692DAC', '#9D538E', '#34ACAF'
	, '#F16745', '#FFC65D', '#7BC8A4', '#4CC3D9', '#93648D', '#692DAC', '#9D538E', '#34ACAF'
	, '#F16745', '#FFC65D', '#7BC8A4', '#4CC3D9', '#93648D', '#692DAC', '#9D538E', '#34ACAF'
	, '#F16745', '#FFC65D', '#7BC8A4', '#4CC3D9', '#93648D', '#692DAC', '#9D538E', '#34ACAF'

];

function getUserData(userId, callback){
	db.model('Challenge').find({'players.userId':userId}, function(er, challenges){
		var res = [];
		for (var i = 0; i < challenges.length; i++) {
			var c = challenges[i];
			var player = c.players.filter(function(p){
				return p.userId == userId;
			}).pop();
			var o = {
				id: c.id
				, name:c.name
				, startDate: c.startDate
				, endDate: c.endDate
				, status: c.status
				, playerCount: c.players.length
				, player: player
			}
			res.push(o);
		};
		callback(er, res);
	});
}

function acceptInvite(inviteId, user, callback){
	var result = {success:false, message:'', code:0, challengeId: null}

	function finish(){
		callback(result);
	}

	db.model('Invite').findOne({id:inviteId}).exec(function(er, invite){
		if (invite != null) {
			db.model('Challenge').findOne({id:invite.challengeId}, function(er, challenge){

				var already = challenge.players.filter(function(p){
					return p.userId == user.id;
				}).pop();

				var color = colors[challenge.players.length];
				var initials = user.name.toUpperCase().substring(0,1);
				var namear = user.name.toUpperCase().replace('  ',' ').split(' ');
				if (namear.length > 1)
					initials += namear[1].toString();

				if (already){
					result.success = true;
					finish();
				}
				else if (invite.email != null && invite.email.toLowerCase() == user.email.toLowerCase()){
					invite.status = 'Accepted';
					invite.save(function(er){
						if (er) {
							result.code = 500;
							result.message = er;
							finish();
						}
						else {
							challenge.players.push({
								id:shortid.generate()
								, name: user.name
								, userId: user.id
								, initials: initials
								, canDos:[]
								, totalCanDoCount:0
								, totalDoneItCount: 0
								, totalPercent:0
								, minutesPerWeek: 0
								, hoursPerWeek: 0
								, rank:99
								, color: color

							})
							challenge.save(function(er){
								if (er) {
									result.code = 404;
									result.message = er;
									finish();
								}
								else {
									result.challengeId = challenge.id;
									result.success = true;
									finish();
								}
							})
							
						}
					})
				}
				else if (invite.mobile != null && invite.mobile == req.user.mobile){
					invite.status = 'Accepted';
					invite.save(function(er){
						if (er) {
							result.code = 500;
							result.message = er;
							finish();
						}
						else {
							challenge.players.push({
								id:shortid.generate()
								, name: req.user.name
								, userId: req.user.id
								, canDos:[]
								, totalCanDoCount:0
								, totalDoneItCount: 0
								, totalPercent:0
								, minutesPerWeek: 0
								, hoursPerWeek: 0
								, rank:99
							})
							challenge.save(function(er){
								if (er) {
									result.code = 404;
									result.message = er;
									finish();
								}
								else {
									result.challengeId = challenge.id;
									result.success = true;
									finish();
								}
							})
							
						}
					})
				}
				else 
				{
					result.code = 404;
					finish();
				}
			})
			
			
		} 
		else {
			result.code = 404;
			finish();
		}
	});
}

function emailExists(email, callback){
	db.model('User').find({email:email}, function(er, emails){
		if (er)
			callback(er);
		else
			callback(null, emails.length > 0);
	})
}

function getUserChallenges(userId, callback){
	db.model('Challenge').find({'players.userId':userId}).where('status').ne('Completed').exec(function(er,data){
		if (er) callback(er);
		else 
			callback(null, data);
	});
}

var data = {
	challenge: db.model('Challenge')
	, user: db.model('User')
	, invite: db.model('Invite')
	, db: db
	, getUserData: getUserData
	, emailExists: emailExists
	, userChallenges: getUserChallenges
	, acceptInvite:acceptInvite
}


module.exports = data;