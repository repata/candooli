
const mongoose = require('mongoose');

module.exports = function(){
  mongoose.model('Challenge', require('./challenge.js'));
  mongoose.model('User', require('./user.js'));
  mongoose.model('Invite', require('./invite.js'));
}